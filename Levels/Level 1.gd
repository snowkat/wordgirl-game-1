extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Secret_Exit_body_entered(body):
	if body.is_in_group('Players'):
		get_tree().change_scene("res://Secret Levels/Node2D.tscn")


func _on_Exit_2_body_entered(body):
	if body.is_in_group('Players'):
		get_tree().change_scene("res://Levels/Flying map.tscn")


func _on_Exit_3_body_entered(body):
	if body.is_in_group('Players'):
		get_tree().change_scene("res://Levels/Level 2.tscn")
