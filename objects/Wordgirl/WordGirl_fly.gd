extends KinematicBody2D
 
export var gravity = 300
export var fly_power = 10000
var velocity = Vector2.ZERO
 
func _ready():
	pass # Replace with function body.
 
 
func _physics_process(delta):
	
	if Input.is_action_just_pressed("player_fly"):
		velocity.y -= fly_power * delta
	else:
		velocity.y += gravity * delta
		
	if velocity.y < -150:
		velocity.y = -150
	move_and_collide(velocity * delta)
